import { pipe } from "fp-ts/function"
import * as O from "fp-ts/Option"
import * as c from "parser-ts/char"
import * as P from "parser-ts/Parser"

const pathSegmentP = pipe(
  c.char("/"),
  P.chain((_) => P.optional(c.char(":"))),
  P.bindTo("urlParamToken"),
  P.bind("segment", (_) => c.many(c.notOneOf(":&/? #[]@"))),
  P.map(({ urlParamToken, segment }) => `${O.getOrElse(() => "")(urlParamToken)}${segment}`),
)

const queryParameterP = pipe(
  c.many1(c.alphanum),
  P.bindTo("name"),
  P.chainFirst((_) => c.char("=")),
  P.bind("value", (_) =>
    c.many(
      P.either(
        c.alphanum,
        // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/encodeURIComponent
        // comma is not listed as un-encoded, as it's apparently reserved though unused.
        // we use it to delimit array query params, though, so it's included here.
        () => c.oneOf(",-_.!~*'()%"),
      ),
    ),
  ),
)

const queryParametersP = pipe(c.char("?"), P.apSecond(P.sepBy1(c.char("&"), queryParameterP)))

/**
 * @internal
 */
export const pathP = pipe(
  P.many(pathSegmentP),
  P.bindTo("pathSegments"),
  P.bind("query", (_) => {
    const endOfURL = P.expected(P.eof<string>(), "end of URL")

    const optionalQueryParametersP = pipe(queryParametersP, P.apFirst(endOfURL), P.map(O.some))

    return pipe(
      optionalQueryParametersP,
      P.alt(
        () =>
          pipe(
            endOfURL,
            P.map((_) => O.none),
          ) as typeof optionalQueryParametersP,
      ),
    )
  }),
)
